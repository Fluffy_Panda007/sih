# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from userview.models import Profile


class Task(models.Model):
    title=models.CharField(max_length=20)
    description = models.CharField(max_length=512)
    rating=models.IntegerField()
    target_time =models.DateTimeField()
    #is_completed=models.BooleanField(default=0)
    completed_at=models.DateTimeField()
    assigned_to=models.ForeignKey(Profile,related_name='given_to')
    assigned_by=models.ForeignKey(Profile,related_name='given_by')
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)


class SubTask(models.Model):
    task=models.ForeignKey(Task,related_name="main_task")
    name=models.CharField(max_length=20)
    description= models.CharField(max_length=512)
    target_time = models.DateTimeField()
    completed_at = models.DateTimeField()
    is_active = models.BooleanField(default=True)

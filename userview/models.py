# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from StringIO import StringIO

import qrcode as qrcode
from django.contrib.auth.models import UserManager
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models

# Create your models here.
from django.db.models import Q

from userview.utils import get_hash


class Department(models.Model):
    ENGINEERING = 0
    ANALTICS = 1
    TECH_SUPPORT = 2
    TYPE_CHOICES = ((0, ENGINEERING), (1, ANALTICS), (2, TECH_SUPPORT))
    department_name = models.CharField(max_length=256, unique=True)
    type = models.IntegerField(choices=TYPE_CHOICES, default=ENGINEERING)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ('department_name',)

    def __unicode__(self):
        return self.department_name

class ProfileManager(UserManager):
    def get_queryset(self):
        return super(ProfileManager, self).get_queryset().filter(user__is_active=True)

class Profile(models.Model):
    MALE = 0
    FEMALE = 1
    TRANSGENDER = 2
    GENDER_CHOICE = ((MALE, 'Male'), (FEMALE, 'Female'))
    user = models.OneToOneField('auth.User', related_name='profile')
    mobile = models.CharField(max_length=10, null=True, blank=True)
    department = models.ForeignKey(Department, related_name='users')
    gender = models.IntegerField(choices=GENDER_CHOICE, null=True)
    empid = models.CharField(max_length=10)
    qrcode_hash = models.CharField(max_length=1024, null=True, blank=True)
    qrcode = models.ImageField(upload_to='qrcode', null=True, blank=True)
    manager_id = models.CharField(max_length=10, null=True, blank=True)
    objects = ProfileManager()


    def is_admin(self):
        return self.user.groups.filter(Q(name='Admin') | Q(name='Developer Admin')).exists()

    def get_total_registrations(self):
        if self.is_admin():
            return Profile.objects.all().count()

    def generte_qr(self):
        hash_string = get_hash(self.empid)
        qr = qrcode.QRCode
        qr.hash = hash_string
        image = qrcode.make(hash_string)
        image = image.convert("RGBA")
        image_io = StringIO()
        image.save(image_io, 'png')
        image_file = InMemoryUploadedFile(image_io, None, 'qr_code.png', 'image/png',image_io.len, None)
        self.qrcode.save('qr_code.png', image_file)
        self.save()

    def __str__(self):
        return self.user.email
